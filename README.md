# newlicense
Add LICENSE files to projects.

Stop copy-pasting and just use newlicense.

## Usage
```
$ ls
Cargo.lock  Cargo.toml  README.md  src

$ newlicense Rust  # prints header to stdout and creates LICENSE file
SPDX-License-Identifier: MIT OR Apache-2.0

Copyright 2021 Ma_124 <ma_124+oss@pm.me>

Licensed under either of
   Apache License, Version 2.0 or
   MIT License
at your option.

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the Apache-2.0
License, shall be dual licensed as above, without any additional terms
or conditions.

$ ls
Cargo.lock  Cargo.toml  LICENSE  README.md  src

$ cd /somewhere/else
$ newlicense -c '// ' MPL2
// SPDX-License-Identifier: MPL-2.0
//
// Copyright 2021 Ma_124 <ma_124+oss@pm.me>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

$ head -n4 LICENSE
Mozilla Public License Version 2.0
==================================

1. Definitions
```

## Installation
```sh
git clone https://gitlab.com/Ma_124/newlicense.git && cd newlicense
cargo build --release --all-features
mv target/release/newlicense ~/.local/bin
```

or build the [PKGBUILD](https://gitlab.com/Ma_124/newlicense/-/raw/package/PKGBUILD?inline=false) on Arch.

## Included Licenses
- Apache-2.0
- BSD-2-Clause
- GPL-3.0
- MIT
- Rust (MIT OR Apache-2.0)
- MPL-2.0

For an already installed version call `newlicense list` for a complete list.

## License
Copyright &copy; 2021 Ma_124
[License](./LICENSE) (generated using `newlicense MIT`)

