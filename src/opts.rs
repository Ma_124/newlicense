use structopt::StructOpt;

pub const LICENSE_NAMES: &[&str] = &[
    "Apache-2.0",
    "BSD-2-Clause",
    "GPL-3.0-only",
    "GPL-3.0-or-later",
    "MIT",
    "Rust",
    "MPL-2.0",
];

/// Add LICENSE files to projects
#[derive(StructOpt, Debug)]
#[structopt(name = "newlicense")]
pub struct Options {
    /// The copyright holder
    #[structopt(short, long, default_value = "Ma_124 <ma_124+oss@pm.me>")]
    pub me: String,

    /// Prefix used for comments
    #[structopt(short, long, default_value = "")]
    pub comment: String,

    /// Overwrite existing LICENSE files
    #[structopt(short, long)]
    pub force: bool,

    /// Only output errors to stdio.
    #[structopt(short, long)]
    pub quiet: bool,

    /// The license to use or `list` to list supported licenses
    #[cfg_attr(not(newlicense_nonbuild), structopt(name = "LICENSE", possible_values = LICENSE_NAMES, hide_possible_values = true))]
    #[cfg_attr(any(newlicense_nonbuild), structopt(name = "LICENSE"))]
    pub license: String,
}
