mod opts;

pub use opts::*;

use chrono::{Datelike, Utc};
use err_derive::Error;
use std::fs::OpenOptions;
use std::io::Write;
use std::{io, process};
use structopt::StructOpt;

struct License {
    id: &'static str,
    aliases: &'static [&'static str],
    header: &'static str,
    text: &'static str,
    header_needs_copyright: bool,
    text_needs_copyright: bool,
}

const LICENSES: &'static [License] = &[
    License {
        id: "Apache-2.0",
        aliases: &["Apache-2"],
        header: include_str!("licenses/Apache-2.0.header.txt"),
        text: include_str!("licenses/Apache-2.0.txt"),
        header_needs_copyright: true,
        text_needs_copyright: false,
    },
    License {
        id: "BSD-2-Clause",
        aliases: &["BSD-2"],
        header: include_str!("licenses/BSD-2-Clause.txt"),
        text: include_str!("licenses/BSD-2-Clause.txt"),
        header_needs_copyright: true,
        text_needs_copyright: true,
    },
    License {
        id: "GPL-3.0-only",
        aliases: &["GPL-3", "GPL-3-only"],
        header: include_str!("licenses/GPL-3.0-only.header.txt"),
        text: include_str!("licenses/GPL-3.0.txt"),
        header_needs_copyright: true,
        text_needs_copyright: false,
    },
    License {
        id: "GPL-3.0-or-later",
        aliases: &["GPL-3-or-later", "GPL-3-later", "GPL-3+"],
        header: include_str!("licenses/GPL-3.0-or-later.header.txt"),
        text: include_str!("licenses/GPL-3.0.txt"),
        header_needs_copyright: true,
        text_needs_copyright: false,
    },
    License {
        id: "MIT",
        aliases: &[],
        header: include_str!("licenses/MIT.txt"),
        text: include_str!("licenses/MIT.txt"),
        header_needs_copyright: true,
        text_needs_copyright: true,
    },
    License {
        id: "MIT OR Apache-2.0",
        aliases: &["Rust"],
        header: include_str!("licenses/Rust.header.txt"),
        text: include_str!("licenses/Rust.txt"),
        header_needs_copyright: true,
        text_needs_copyright: true,
    },
    License {
        id: "MPL-2.0",
        aliases: &["MPL-2"],
        header: include_str!("licenses/MPL-2.0.header.txt"),
        text: include_str!("licenses/MPL-2.0.txt"),
        header_needs_copyright: true,
        text_needs_copyright: false,
    },
];

#[derive(Error, Debug)]
enum Error {
    #[error(
        display = "unknown license {:?}. Call `newlicense list` for a complete list of available licenses.",
        _0
    )]
    NoSuchLicense(String),

    #[error(display = "{} ({})", _0, _1)]
    IoError(&'static str, #[error(source, no_from)] io::Error),
}

trait IoErrorExt {
    type E;

    fn with_context(self, context: &'static str) -> Self::E;
}

impl IoErrorExt for io::Error {
    type E = Error;

    fn with_context(self, context: &'static str) -> Self::E {
        Error::IoError(context, self)
    }
}

impl<T> IoErrorExt for Result<T, io::Error> {
    type E = Result<T, Error>;

    fn with_context(self, context: &'static str) -> Self::E {
        self.map_err(|e| e.with_context(context))
    }
}

fn main() {
    process::exit(match main_impl() {
        Ok(_) => 0,
        Err(e) => {
            eprintln!("{}", e);
            1
        }
    })
}

fn main_impl() -> Result<(), Error> {
    let opts: Options = Options::from_args();
    if norm_name(&opts.license) == "list" {
        println!("These licenses are included in this version of newlicense:");
        for l in LICENSES {
            println!("- {}: {}", l.id, l.aliases.join(", "))
        }
        println!(
            r"
Matching for license names is case-insensitive and ignores most
punctuation (notably `-` and `.` are ignored but not `+`).  In automatic
scripts the full SPDX identifier (e.g. `GPL-3.0-only`) or the first
alias of an SPDX compound (e.g. `Rust` instead of `MIT OR Apache-2.0`)
should be preferred, as future releases may remove or modify any other
aliases and matching rules."
        );
        return Ok(());
    }

    let license =
        get_license(&opts.license).ok_or_else(|| Error::NoSuchLicense(opts.license.clone()))?;

    let mut f = OpenOptions::new()
        .write(true)
        .create(true)
        .create_new(!opts.force)
        .truncate(opts.force)
        .open("LICENSE")
        .with_context("cannot open LICENSE file")?;

    let mut copyright = String::new();

    if license.text_needs_copyright | license.header_needs_copyright {
        copyright = format!("Copyright {} {}\n\n", Utc::now().year(), opts.me);
    }

    const WRITE_ERR_CONTEXT: &str = "cannot write to LICENSE file";
    if license.text_needs_copyright {
        f.write_all(copyright.as_bytes())
            .with_context(WRITE_ERR_CONTEXT)?;
    }
    f.write_all(license.text.as_bytes())
        .with_context(WRITE_ERR_CONTEXT)?;

    println!(
        "{com}SPDX-License-Identifier: {id}\n{com}",
        com = opts.comment,
        id = license.id
    );
    if license.header_needs_copyright {
        println!("{com}{}\n{com}", copyright.trim_end(), com = opts.comment);
    }
    println!(
        "{com}{}",
        license.header.replace("\n", &format!("\n{}", opts.comment)),
        com = opts.comment
    );

    Ok(())
}

fn get_license(name: &str) -> Option<&'static License> {
    let name = norm_name(name);
    for l in LICENSES {
        if name == norm_name(l.id) {
            return Some(l);
        }
        for alias in l.aliases {
            if name == norm_name(alias) {
                return Some(l);
            }
        }
    }
    None
}

fn norm_name(orig: &str) -> String {
    let mut normed = String::with_capacity(orig.len());
    for c in orig.chars() {
        if c.is_ascii_uppercase() {
            normed.push(c.to_ascii_lowercase());
        } else if c.is_ascii_lowercase()
            || c.is_ascii_digit()
            || matches!(c, '+' | '|' | '&' | '(' | ')' | '?')
        {
            normed.push(c);
        }
    }
    normed
}

#[cfg(test)]
mod tests {
    use crate::{LICENSES, LICENSE_NAMES};

    #[test]
    fn test_license_names() {
        assert_eq!(LICENSES.len(), LICENSE_NAMES.len());
        for i in 0..LICENSES.len() {
            if LICENSES[i].id == LICENSE_NAMES[i] {
                continue;
            }
            if let Some(alias) = LICENSES[i].aliases.first() {
                if alias == &LICENSE_NAMES[i] {
                    continue;
                }
            }
            assert_eq!(LICENSES[i].id, LICENSE_NAMES[i])
        }
    }
}
