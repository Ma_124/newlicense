use clap::Shell;
use std::env;

include!("src/opts.rs");

fn main() {
    println!("cargo:rustc-cfg=newlicense_nonbuild");
    println!("cargo:rerun-if-env-changed=_NEWLICENSE_COMPLETIONS");

    let outdir = match env::var_os("_NEWLICENSE_COMPLETIONS") {
        None => return,
        Some(outdir) => outdir,
    };

    Options::clap().gen_completions("newlicense", Shell::Bash, &outdir);
    Options::clap().gen_completions("newlicense", Shell::Fish, &outdir);
    Options::clap().gen_completions("newlicense", Shell::Zsh, &outdir);
    Options::clap().gen_completions("newlicense", Shell::PowerShell, &outdir);
    Options::clap().gen_completions("newlicense", Shell::Elvish, &outdir);
}
